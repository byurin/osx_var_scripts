#!/bin/bash
# Once this script has run, you must setup screensavers URL by yourself
# under System Preferences/Desktop & Screen Saver/WebViewScreenSaver/Options
# You may add a shortcut of your choice under :
# System Preferences/Keyboard/Shorcuts/Services/42_safe_lock
# Beware that some shortcut choices might not work.
# (I recommend 'CMD+SHIFT+L')

set -e

git clone https://bitbucket.org/byurin/osx_var_scripts/42_safe_lock ~/Downloads/42_safe_lock

cd ~/Downloads/42_safe_lock/42_safe_lock

if [ ! -e ~/Library/Screen\ Savers/WebViewScreenSaver.saver ]; then
	curl -L https://github.com/liquidx/webviewscreensaver/releases/download/v2.0/WebViewScreenSaver-2.0.zip --output ./WebViewScreenSaver.zip
	tar -xvf ./WebViewScreenSaver.zip
	mv WebViewScreenSaver.saver ~/Library/Screen\ Savers/
	rm WebViewScreenSaver.zip
fi

cp -r 42_safe_lock.workflow ~/Library/Services

mkdir ~/.42_safe_lock/

chmod 755 42_safe_lock.sh

mv 42_safe_lock.sh ~/.42_safe_lock/
