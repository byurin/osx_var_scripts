#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
import Quartz
d=Quartz.CGSessionCopyCurrentDictionary()
if (d and d.get("CGSSessionScreenIsLocked", 0) == 1):
    exit(1)
else:
    exit(0)
