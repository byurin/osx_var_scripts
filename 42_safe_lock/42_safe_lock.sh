#!/bin/bash

# Lock current user session and starts random screensaver
# At the same time starts a countdown from provided COUNTDOWN variable
# When the countdown reaches 0, current session is logged out
# WARNING : Opened apps mustn't block the logout for this script to work
# (For Iterm (Iterm->Preferences->General->Closing->Confirm "Quit Iterm"...)
# Scripts also depends on Python to be installed

if [ $1 ]; then
	COUNTDOWN=$1
else
	COUNTDOWN=41
fi

lock_screen()
{
	osascript -e 'tell application "System Events"
		set ss to screen saver "WebViewScreenSaver"
		start ss
	end tell'
}

is_locked()
{
python -c "(lambda __print, __g: [[[[(lambda __after: (__print(1), __after())[1] if (d and (d.get('CGSSessionScreenIsLocked', 0) == 1)) else (__print(0), __after())[1])(lambda: None) for __g['d'] in [(Quartz.CGSessionCopyCurrentDictionary())]][0] for __g['Quartz'] in [(__import__('Quartz', __g, __g))]][0] for __g['os'] in [(__import__('os', __g, __g))]][0] for __g['sys'] in [(__import__('sys', __g, __g))]][0])(__import__('__builtin__', level=0).__dict__['print'], globals())"
}

# Get color from green to red (by assuming a given COUNTDOWN of '42')
get_color()
{
	local color
	if [ $1 -ge 21 ]; then
		color=40
	elif [ $1 -ge 10 ]; then
		color=184
	elif [ $1 -ge 5 ]; then
		color=214
	else
		color=196
	fi
	echo $color
}

# Countdowns from provided argument to 0 by displaying MM:SS
# Argument is believed to be provided as minutes
minutor()
{
	TIME=$1
	let TIME=TIME-1
	for m in $(seq $TIME 0); do
		color=$(get_color $m)
		for s in {59..00}; do
			printf "\e[38;05;%dm%02d:%02d\e[0m\r" $color $m $s;
			sleep 1;
		done;
	done;
}

printf "Running logout script...\n"
lock_screen
minutor $COUNTDOWN
if [ $(is_locked) -eq 1 ]; then
	osascript -e 'tell app "loginwindow" to «event aevtrlgo»'
else
	printf 'Bravo, you came back in time...\n'
fi
